print("Hello, world!") # will display "Hello, World" in the terminal

# This is a comment

# Variables and Datatypes

# Strings
full_name = "John Doe"
secret_code = "Pa$$w0rd"

# Numbers (int, float, complex)
num_of_days = 365 # this is an integer
pi_approx = 3.1416 # this is a decimal
complex_num = 1 + 5j # this is a complex, j represents the imaginary component

# Boolean - truth values
isLearning = True
isDifficult = False

# Naming conventions
# The terminology used for variable names is identifier.
# All identifiers should begin with a letter(A to Z or a to z), dollar sign($) or an underscore.
# After the first character, identifiers can have any combination of characters.
# Unlike JavaScript that uses camel case, Python uses the snake case convention for variables as defined in the PEP (Python Enhancement Proposal) 8 - Style Guide for Python Code
# A keyword cannot be used as an identifier.
# Most importantly, identifiers are case sensitive.

# Assigning values to Multiple Variables
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# Concatenating variables
print("My name is " + full_name)

# str() - converts values to string
# int() - converts values to integer
# float() - converts values to float
age = 25
print("My age is " + str(age))
print(int(3.5))
print(int("9856"))

# Arithmetic Operations - +, -, *, /, %

# Assignment Operators = used to assign values to variables
# -=, *=, /=, %=
num1 = 3
num1 += 4 # num1 = num1 + 4
print(num1)

# Comparison operators - ==, !=, >, <, >=, <=
print(1 == 1)

# Logical Operators - used to combine conditional statements
# and, or, not

print(True and False)
print(False or True)
print(not False)

# Activity
#1. Create 5 variables and output them in the terminal in the following format:

#"I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

# Sample output:
# I am John Smith, and I am 25 years old, I work as a web developer, and my rating for Avengers is 8.5

#2. Perform the following statements

#Create 3 variables, num1, num2, and num3

#Get the product of num1 and num2

#Check if num1 is less than num3

#Add the value of num3 to num2