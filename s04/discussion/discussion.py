# Lists - they can contain collection of data
names = ["John", "Paul", "George", "Ringo"] #String list
programs = ['developer career', 'pi-shape', 'short courses']
durations = [260, 180, 20] #number list
truth_values = [True, False, True, True, False] #Boolean list

# Lists that can contain elements of different data types:
sample_list = ["Apple", 3, False, "Potato", True]

# Getting the list size - len() method
print(len(programs))

# Accessing values
# Accessing the first item in the list
print(programs[0])
#print(programs[-3]) pi-shape

# Accessing the whole list
print(durations)

# Accessing a range of values
# list[start_index: end_index]
print(programs[0:2]) # will display index 0 and 1

# [Section] Mini exercise:
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing in the following format:
students = ["Mary", "Matthew", "Tom", "Anna", "Thomas"]
grades = [100, 85, 88, 90, 75]

count = 0
while count < len(students):
	print(f"The grade of {students[count]} is { grades[count] }")
	count += 1

# Updating lists
# Print the current value
print(f'Current value: {programs[2]}')

#update the value
programs[2] = 'Elective courses'

# Print the new value
print(f'New value: {programs[2]}')

# List Manipulation
# Adding list items - the append() method allows us to insert items to the list
programs.append('global')
print(programs)

# Deleting items in the list - the "del" keyword can be used to delete elements in the list
durations.append(360)
print(durations)

# Delete the last item on the list
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if the element is in the list
# return true or false
print(20 in durations)
print(500 in durations)

# Sorting lists - the sort() method sorts the list alphanumerically, ascending by default
names.sort()
print(names)

# Emptying the list - the clear() method is used to empty the contents of the list
test_list = [1, 2, 3, 4, 5]
test_list.clear()
print(test_list)

# Dictionaries are used to store data values in key:value pairs.
person1 = {
	"name" : "Brandon",
	"age" : 28,
	"occupation" : "student",
	"isEnrolled" : True,
	"subjects" : ["Python", "SQL", "Django"]
}

print(len(person1))

# Accessing values in the dictionary
print(person1["name"])

# keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# values() method will return a list of all the values in the dictionary
print(person1.values())

# items() method will return each item in a dictionary, as key-value pair in a list
print(person1.items())

# update() method will add key-value pairs putting a new index in the dictionary
person1["nationality"] = "Nepalese"
person1.update({"fav_food" : "momo"})
print(person1)

# Deleting entries can be done using the pop() method or the del keyword
person1.pop("fav_food")
del person1["nationality"]
print(person1)

# The clear() method empties the dictionary
person2 = {
	"name" : "John",
	"age" : 18
}
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
	print(f"The value of the {key} is {person1[key]}")