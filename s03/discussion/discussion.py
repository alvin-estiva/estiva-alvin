# The input() method allows the users to give inputs to the program
#username = input("Please enter your name: \n")
#print(f"Hello {username}! Welcome to the Python short course!")

# Only accepts string type
#num1 = input("Enter 1st number:\n")
#num2 = input("Enter 2nd number:\n")
#print(f"The sum of num1 and num2 is {num1 + num2}")

#using int() will typecast string to integer(number)
#num1 = int(input("Enter 1st number:\n"))
#num2 = int(input("Enter 2nd number:\n"))
#print(f"The sum of num1 and num2 is {num1 + num2}")

#Control Structures

# Selection control structures
# If-else statements

test_num = 75

if test_num >= 60:
	print("Test passed")
else:
	print("Test failed")

test_num2 = int(input("Please enter the 2nd test number\n"))
if test_num2 > 0:
	print("The number is positive")
elif test_num2 == 0:
	print("The number is zero")
else:
	print("The number is negative")

	# Mini Exercise 1:
	    # Create an if-else statement that determines if a number is divisible by 3, 5, or both. 
	    # If the number is divisible by 3, print "The number is divisible by 3"
	    # If the number is divisible by 5, print "The number is divisible by 5"
	    # If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
	    # If the number is not divisible by any, print "The number is not divisible by 3 nor 5"

test_div_num = int(input("Please enter a number to test:\n"))
if test_div_num % 3 == 0 and test_div_num % 5 == 0:
	print(f"{test_div_num} is divisible by both 3 and 5")
elif test_div_num % 3 == 0:
	print(f"{test_div_num} is divisible by 3")
elif test_div_num % 5 == 0:
	print(f"{test_div_num} is divisible by 5")
else:
	print(f"{test_div_num} is not divisible by both 3 nor 5")

# Repetition control structures(Loops)
# While loops
i = 1
while i <= 5:
	print(f"Current count {i}")
	i += 1

# For Loop
fruits = ["apple", "banana", "cherry"]
for indiv_fruit in fruits:
	print(indiv_fruit)

# range() method
#for x in range(6):
#	print(f"The current value is {x}")

#for x in range(6, 10):
#	print(f"The current value is {x}")

for x in range(6, 20, 2):
	print(f"The current value is {x}")

# Break statement - used to stop the loop
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j += 1

# Continue statement - returns the control to the beginning of the while loop and continue with the next iteration

k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k)